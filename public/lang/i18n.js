import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import LanguageDetector from "i18next-browser-languagedetector";
 
import { TRANSLATIONS_ID } from "./IDN/translatations";
import { TRANSLATIONS_EN } from "./EN/translatations";
 
i18n
 .use(LanguageDetector)
 .use(initReactI18next)
 .init({
   resources: {
     id: {
       translation: TRANSLATIONS_ID
     },
     en: {
       translation: TRANSLATIONS_EN
     }
   }
 });
 
i18n.changeLanguage("id");