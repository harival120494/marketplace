import React, { useEffect, useState } from "react";
import { 
    Modal,
    Button
} from 'react-materialize';
import {Navigate} from 'react-router-dom'
import {Menu} from '@styled-icons/remix-line/Menu';
import {LogoutCircleR} from '@styled-icons/remix-line/LogoutCircleR';
import {Language} from '@styled-icons/ionicons-outline/Language';
import { useDispatch, useSelector} from "react-redux";
import {act_navbar} from '../../redux/actions/TemplateActions';
import { actLogin } from "../../redux/actions/LoginActions";

function NavBar(){
    

    const dispatch = useDispatch();
    const isSideShow = useSelector((state) => state.TemplateReducer.isSideShow);
    const [logoutModal, setLogoutModal] = useState(false);

    const logout = () => {
        const data = {
            username : '',
            password : '',
            role : '',
            user_id : '',
            token : ''
        };
        dispatch(actLogin(data));
        <Navigate to="/login" />
    }

    return(
        <>
            <nav className="top-nav">
                <div className="container">
                <div className="nav-wrapper">
                    <div className="row">
                        <div className="col s6 m1 offset-m1" style={{cursor:'pointer'}} onClick={() => dispatch(act_navbar(!isSideShow))}>
                            &nbsp;&nbsp; <Menu size="30"/>
                        </div>
                    </div>                        
                </div>
                </div>
                <div className="logout-btn-wrapper">
                    <span className="logout-btn" onClick={() => setLogoutModal(true)}>
                        Logout &nbsp; <LogoutCircleR size={20}/>
                    </span>
                </div>
                <div className="lang-btn-wrapper">
                    <Language size={20} style={{float:'left',marginTop:'.4rem', marginLeft:'5px'}}/>
                    <select class="browser-default lang white-text" style={styles.select_paket}>
                        <option value="1">IDN</option>
                        <option value="2">ENG</option>
                    </select>
                </div>
            </nav>

            <Modal
                actions={[
                    <Button flat modal="close" node="button" waves="green" onClick={() => setLogoutModal(false)}>Close</Button>,
                    <Button waves="light" onClick={() => logout()}>Yes <LogoutCircleR size={25}/></Button>
                ]}
                bottomSheet={false}
                fixedFooter={false}
                header=""
                id="modal1"
                open={logoutModal}
                options={{
                dismissible: false,
                endingTop: '10%',
                inDuration: 250,
                onCloseEnd: null,
                onCloseStart: null,
                onOpenEnd: null,
                onOpenStart: null,
                opacity: 0.5,
                outDuration: 250,
                preventScrolling: true,
                startingTop: '4%'
                }}
            >
                <div className="center-align">
                    <h4 mb={5} className="margin bottom-40">Logout</h4>
                    <LogoutCircleR size={50} color="#3b6889" className="margin bottom-20"/>
                    <h6 style={{fontFamily:'Poppins-Light'}}>Are you sure want to logout ?</h6>
                </div>
            </Modal>
        </>
    )
}

const styles = {
    select_paket : {
        borderRadius: 10,
        height:'auto',
        fontFamily: 'Poppins-Light',
        fontSize: 14,
        color: 'rgba(0,0,0, .6)'
    }
}
export default NavBar;