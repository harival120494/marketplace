import React,{ useEffect, useState }  from "react";
import { 
    SideNavItem,
    Col
} from 'react-materialize';
import {Link} from'react-router-dom'
import {InfoShield} from '@styled-icons/fluentui-system-filled/InfoShield';
import { Table } from '@styled-icons/bootstrap/Table';
import {Stylelint} from '@styled-icons/simple-icons/Stylelint';
import {MobileVibration} from '@styled-icons/boxicons-regular/MobileVibration';
import {Briefcase} from '@styled-icons/bootstrap/Briefcase';
import {Affiliatetheme} from '@styled-icons/fa-brands/Affiliatetheme';
import { useSelector} from "react-redux";

function SideNav(){
    const isSideShow = useSelector((state) => state.TemplateReducer.isSideShow);
    const [subMenu, setSubMenu] = useState(false);
    useEffect(() => {

    }, []);

    
    return(
        <>
            <ul id="nav-mobile" className={`sidenav ${ isSideShow ? 'sidenav-fixed' : 'sidenav-out'}`}>
                <div className="row margin top-20 valign-wrapper">
                    <div className="col s12 m4 center-on-small-only">
                        <div className="image-container">
                        <img src="https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8dXNlciUyMHByb2ZpbGV8ZW58MHx8MHx8&w=1000&q=80" width={200} className="circle responsive-img valign"/>
                        </div>
                    </div>
                    <div className="col s12 m8 ">
                        <strong className="white-text" style={{
                            fontSize:12.5, fontFamily:'Poppins-Bold'
                        }}>Halo,</strong><br/>
                        <span className="white-text" style={{fontSize:13, fontFamily:'Poppins-Light', letterSpacing:2}}>Harival Tivani</span>
                    </div>
                </div>
                <div className="row ">
                    <Col xl={12}>
                        <select class="browser-default" style={styles.select_paket}>
                            <option value="" disabled selected>Choose your option</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>
                        </select>
                    </Col>
                </div>
                <SideNavItem divider />
                <SideNavItem subheader>
                    Menu
                </SideNavItem>
                <li className="bold"><Link to="/admin/dashboard" className="waves-effect waves-teal"><InfoShield size={20}/> &nbsp; Dashboard</Link></li>
                <li className="bold"><Link to="/admin/tables" className="waves-effect waves-teal"><Table size={20}/> &nbsp; Tables</Link></li>
                <li className="no-padding">
                <ul className="collapsible collapsible-accordion">
                    <li className="bold"><a className="collapsible-header waves-effect waves-teal" tabIndex="0" onClick={(e) => setSubMenu(!subMenu)}><Stylelint size={20}/> &nbsp; CSS</a>
                    {
                        subMenu && 
                        (
                            <div className="collapsible-body" style={{background:'none', display:'block', transition:'all ease-in .5s'}}>
                                <ul>
                                <li><a href="color.html">Color</a></li>
                                <li><a href="grid.html">Grid</a></li>
                                <li><a href="helpers.html">Helpers</a></li>
                                <li><a href="media-css.html">Media</a></li>
                                <li><a href="pulse.html">Pulse</a></li>
                                <li><a href="sass.html">Sass</a></li>
                                <li><a href="shadow.html">Shadow</a></li>
                                <li><a href="table.html">Table</a></li>
                                <li><a href="css-transitions.html">Transitions</a></li>
                                <li><a href="typography.html">Typography</a></li>
                                </ul>
                            </div>
                        )
                    }
                    </li>
                </ul>
                </li>
                <li className="bold"><a href="mobile.html" className="waves-effect waves-teal"><MobileVibration size={20}/> &nbsp; Mobile</a></li>
                <li className="bold"><a href="showcase.html" className="waves-effect waves-teal"><Briefcase size={20}/> &nbsp; Showcase</a></li>
                <li className="bold"><a href="themes.html" className="waves-effect waves-teal"><Affiliatetheme size={20}/> &nbsp; Themes</a></li>
            </ul>
        </>
    )
}


const styles = {
    select_paket : {
        borderRadius: 10,
        height:'auto',
        fontFamily: 'Poppins-Light',
        fontSize: 14,
        color: 'rgba(0,0,0, .6)'
    }
}

export default SideNav;