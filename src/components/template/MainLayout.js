import React, { useEffect } from "react";
import { 
    Container
} from 'react-materialize';
import SideNav from '../../components/template/SideNav';
import NavBar from '../../components/template/NavBar';
import {useSelector} from "react-redux";
import {
    Outlet
  } from "react-router-dom";



function MainLayout(){
    const isSideShow = useSelector((state) => state.TemplateReducer.isSideShow);
    useEffect(() => {
        
    }, []);
    return(
        <>
            <header>
                <NavBar/>
                <SideNav/>
            </header>
            <main>
                <Container>
                    <div className="row">
                        <div className={`col s12 ${isSideShow ? 'offset-m1 x20 offset-xl2' : 'offset-xl2-out'}`}>
                            <Outlet />
                        </div>
                    </div>
                </Container>
            </main>  
        </>
    )
}
export default MainLayout;