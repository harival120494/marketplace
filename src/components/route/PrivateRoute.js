import {Navigate, Outlet} from 'react-router-dom'
import {useSelector} from 'react-redux'



const PrivateWrapper = () => {
    const token = useSelector((state) => state.LoginReducer.token);     // Check if token exist

    return token ? <Outlet /> : <Navigate to="/login" />;
};

export default PrivateWrapper;