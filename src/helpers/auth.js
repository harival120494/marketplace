import React from 'react' 
import {useSelector} from 'react-redux'

function Auth(){
    const token  = useSelector((state) => state.LoginReducer.token);
    return token != '' ? true : false;
}

export default Auth