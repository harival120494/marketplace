import React, { useEffect, useState } from 'react'
import {
    Table,
    Col
} from 'react-materialize'
import LoadingComponent from '../../components/loading'

const TableComponent = () => {
    return(
        <>
            <h5>Tables</h5>
           <p>
           It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
           </p>
           <div className="row">
                <Col xl={12}>
                    <Table>
                    <thead>
                        <tr>
                        <th data-field="id">
                            Name
                        </th>
                        <th data-field="name">
                            Item Name
                        </th>
                        <th data-field="price">
                            Item Price
                        </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <td>
                            Alvin
                        </td>
                        <td>
                            Eclair
                        </td>
                        <td>
                            $0.87
                        </td>
                        </tr>
                        <tr>
                        <td>
                            Alan
                        </td>
                        <td>
                            Jellybean
                        </td>
                        <td>
                            $3.76
                        </td>
                        </tr>
                        <tr>
                        <td>
                            Jonathan
                        </td>
                        <td>
                            Lollipop
                        </td>
                        <td>
                            $7.00
                        </td>
                        </tr>
                    </tbody>
                    </Table>
                </Col>
            </div>
        </>
    )
}

function Tables(){
    const [isMounted, setMounted] = useState(false);
    useEffect(() => {
        setMounted(true);
    },[])
    return(
        <>
           {isMounted ? <TableComponent/> : <LoadingComponent/>}
        </>
    )
}

export default Tables;