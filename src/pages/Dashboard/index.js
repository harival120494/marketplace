import React, { useEffect, useState } from 'react'
import LoadingComponent from '../../components/loading'


const DashboardComponent = () => {
    return (
        <>
            <h5>Dashboard</h5>
           <p>
           It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
           </p>
           <div className="row">
                <form className="col s12">
                <div className="row">
                    <div className="input-field col s6">
                    <input placeholder="Placeholder" id="first_name" type="text" className="validate"/>
                    <label for="first_name">First Name</label>
                    </div>
                    <div className="input-field col s6">
                    <input id="last_name" type="text" className="validate"/>
                    <label for="last_name">Last Name</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                    <input disabled value="I am not editable" id="disabled" type="text" className="validate"/>
                    <label for="disabled">Disabled</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                    <input id="password" type="password" className="validate"/>
                    <label for="password">Password</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                    <input id="email" type="email" className="validate"/>
                    <label for="email">Email</label>
                    </div>
                </div>
                <div className="row">
                    <div className="col s12">
                    This is an inline input field:
                    <div className="input-field inline">
                        <input id="email_inline" type="email" className="validate"/>
                        <label for="email_inline">Email</label>
                        <span className="helper-text" data-error="wrong" data-success="right">Helper text</span>
                    </div>
                    </div>
                </div>
                </form>
            </div>
        </>
    )
}



function Dashboard(){
    const [isMounted, setMounted] = useState(false);
    useEffect(() => {
        setMounted(true);
    },[])
    return(
        <>
           {isMounted ? <DashboardComponent/> : <LoadingComponent/>}
        </>
    )
}

export default Dashboard;