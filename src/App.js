import React, {useEffect, useState} from "react";
import 'materialize-css'
import { Card, Row, Col, Container, TextInput, Button} from 'react-materialize';
import { useDispatch, useSelector } from "react-redux";
import { actLogin } from "./redux/actions/LoginActions";

function App() {

  const dispatch = useDispatch();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  
  useEffect((props) => {

  }, [username, password]);

  const _loginHandler = () => {
    
    const data = {
      username : username,
      password : password,
      role : 1,
      user_id : 231,
      token : '098f6bcd4621d373cade4e832627b4f6'
    };
    dispatch(actLogin(data));
  }
  return(
    <Container>
      <p></p>
        <Row>
          <Col xl={2} m={2} s={12}></Col>
          <Col xl={8} m={8} s={12}>
            <Card
              title="Login Page">
                <Row>
                  <Col xl={12} m={12} s={12}>
                  <TextInput xl={12} label="Username" onChange={(e) => setUsername(e.target.value)}/>
                  </Col>
                </Row>
                <Row>
                  <Col xl={12} m={12} s={12}>
                  <TextInput xl={12} label="Password" password={true} onChange={(e) => setPassword(e.target.value)}/>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Button node="button"
                      style={{
                        marginRight: '5px'
                      }}
                      waves="light"
                      onClick={() => _loginHandler()}
                    >
                      Login
                    </Button>
                  </Col>
                </Row>
            </Card>
          </Col>
          <Col xl={2} m={2} s={12}></Col>
        </Row>
    </Container>
  )
}
export default App;