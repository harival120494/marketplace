import React, {Fragment} from "react";
import { Routes, Route, BrowserRouter} from "react-router-dom";
import "./App.css";
import 'materialize-css/dist/css/materialize.min.css'
import LoginRoute from '../src/components/route/LoginRoute'
import PrivateWrapper from '../src/components/route/PrivateRoute'

import MainLayout from "./components/template/MainLayout";
import {
  Dashboard,
  Tables
}
from './pages'


function Router() {
  return (
    <>
        <Routes>
            <Route path='/login' element={<LoginRoute/>}/>
            <Route path='/test' element={<MainLayout/>}/>
            <Route element={<PrivateWrapper/>}>
              <Route path="/admin" element={<MainLayout />}>
                  <Route path="dashboard" element={<Dashboard />} />
                  <Route path="Tables" element={<Tables />} />
              </Route>
            </Route>
        </Routes>
    </>
  );
}
export default Router;