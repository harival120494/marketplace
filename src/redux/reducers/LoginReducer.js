import { ACT_LOGIN, ACT_LOGOUT } from "../actions/LoginActions";

const initialState = {
    username : '',
    password : '',
    role : '',
    user_id : '',
    token : ''
}

function LoginReducer (state = initialState, action){
   
    switch (action.type) {
        case ACT_LOGIN:
            return {
                ...state, ...action.payload
            }
        case ACT_LOGOUT:
            return {
                ...state, ...action.payload
            }
        default:
            return state;
    }
}

export default LoginReducer;