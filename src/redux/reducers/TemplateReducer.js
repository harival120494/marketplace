import {NAV_BAR} from '../actions/TemplateActions';

const initialState = {
    isSideShow : true
}

function SideNavReducer (state = initialState, action){
    switch (action.type) {
        case NAV_BAR:
            return {...state, isSideShow : action.payload}
    
        default:
            return state;
    }
}

export default SideNavReducer;