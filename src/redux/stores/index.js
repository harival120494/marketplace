import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import LoginReducer from "../reducers/LoginReducer";
import TemplateReducer from '../reducers/TemplateReducer';


const persistConfig = {
    key: "root",
    storage,
};

const middleWares = [thunk];
const rootReducer = combineReducers({LoginReducer, TemplateReducer});
const persistedReducer = persistReducer(persistConfig, rootReducer);
const Store = createStore(persistedReducer, applyMiddleware(...middleWares));
export default Store;