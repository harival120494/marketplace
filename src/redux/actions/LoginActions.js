export const ACT_LOGIN = "ACT_LOGIN";
export const ACT_LOGOUT = "ACT_LOGOUT";

export const actLogin = data => dispatch => {
    
    dispatch({
        type : ACT_LOGIN,
        payload : data
    })
}

export const actLogout = data => dispatch => {
    dispatch({
        type : ACT_LOGOUT,
        payload : data
    })
}